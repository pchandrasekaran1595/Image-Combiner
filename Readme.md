## Command Line Tool to Combine Images

- Read Path = `./Files`
- Write Path = `./Processed`

<br>

## **CLI Arguments**

<br>

- `--file1, -f1` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Filename of the first file
- `--file2, -f2` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Filename of the second file
- `--vertical, -v` &nbsp;&nbsp;&nbsp; - Flag to stack images vertically instead of horizontally
- `--adapt-big, -ab` - Adapt larger image to the the smaller image
- `--save, -s` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Save the Processed Image